[13:45] Alcides Araújo
# Set working directory to be the parent directory of that in which the script is located.
cd "$(dirname "$(dirname "$(readlink -f "$0")")")"# Authenticate.
gcloud auth activate-service-account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
--key-file /home/jupyter/pa_digital_project/prod/src/config/gcloud_credentials_dev.json
# Submit build.
gcloud builds submit \
--tag gcr.io/laboratorio-eng-dados/dev-pa-digital-service \
--account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
--project laboratorio-eng-dados \
--gcs-log-dir gs://dev-ml-model/pa_digital_models/logs/# Deploy and run container.
gcloud run deploy \
dev-pa-digital-service \
--image gcr.io/laboratorio-eng-dados/dev-pa-digital-service \
--allow-unauthenticated \
--platform managed \
--region us-east1 \
--account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
--project laboratorio-eng-dados \
--memory 8Gi \
--timeout 30m \
--cpu 4

