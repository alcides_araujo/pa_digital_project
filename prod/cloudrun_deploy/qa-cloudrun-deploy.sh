# Set working directory to be the parent directory of that in which the script is located.
cd "$(dirname "$(dirname "$(readlink -f "$0")")")"

# Authenticate.
gcloud auth activate-service-account machine-learning-qa@gesto-qa.iam.gserviceaccount.com \
--key-file /home/alcides-araujo/deploy_qa/pa_digital_project/prod/src/config/machine-learning-qa-gesto-qa-dc1e6de3a282.json

# Submit build.
gcloud builds submit \
--tag gcr.io/gesto-qa/dev-pa-digital-service \
--account machine-learning-qa@gesto-qa.iam.gserviceaccount.com \
--project gesto-qa \
--gcs-log-dir gs://qa-ml-model/pa_digital_models/logs/

# Deploy and run container.
gcloud run deploy \
dev-pa-digital-service \
--image gcr.io/gesto-qa/dev-pa-digital-service \
--allow-unauthenticated \
--platform managed \
--region us-east1 \
--account machine-learning-qa@gesto-qa.iam.gserviceaccount.com \
--project gesto-qa \
--memory 2Gi \
--timeout 10m \
--cpu 1

