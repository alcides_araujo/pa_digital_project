# general
import numpy as np
import pandas as pd
import re
from unidecode import unidecode

# text processing
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

# ml models
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score, precision_score, classification_report, recall_score, f1_score, confusion_matrix, balanced_accuracy_score
from sklearn.model_selection import cross_validate
from sklearn.linear_model import LogisticRegression
from sklearn.utils import class_weight
from joblib import dump

# visualization
import matplotlib.pyplot as plt 
import seaborn as sns

# api
import logging


log = logging.getLogger("uvicorn")


# create classes from target variable
def create_target(df, dictionary):
    
    '''Creating classes from dictionary
       df: pandas dataframe 
       dictionary: pandas dataframe with target variable with expressions'''
    
    # create class
    for target in dictionary.columns:
        df.loc[df['tokens'].str.contains('|'.join(dictionary[target])), 'target'] = target
    
    df['target'].fillna('outros',inplace= True)
    
    return df


# train multinomial logistic regression
def training_multinomialLR_process(df, settings):
    
    '''Training the model
    df: final pandas dataframe preprocessed'''
    
    # train/test split
    X = df.tokens
    y = df.target
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify = y, random_state=42, test_size=0.25)
    
    # compute class_weight - Estimate class weights for unbalanced datasets.
    target = df['target'].tolist()   
    classes = np.unique(target)
    class_weights = class_weight.compute_class_weight(class_weight ='balanced', 
                                                      classes = classes, 
                                                      y = target)
    class_weights = dict(zip(classes,class_weights))
    
    # train pipeline
    logreg = Pipeline([('vect', CountVectorizer(max_df=500)),
                       ('tfidf', TfidfTransformer()),
                       ('clf', LogisticRegression(multi_class='multinomial',class_weight=class_weights,max_iter=1000))])
    logreg.fit(X_train, y_train)
    
    ## Save evaluation metrics
    y_pred = logreg.predict(X_test)
    
    # save cross validation
    log.info(f"Cross validation initiated")
    
    evaluation_metrics = ['balanced_accuracy', 'accuracy', 'precision_macro', 'recall_macro', 'f1_macro']
    cross_validation_results = cross_validate(logreg, X,y, cv=5, scoring=evaluation_metrics)
    cross_validation_results = pd.DataFrame.from_dict(cross_validation_results)
    cv_path = f'{settings.ROOT_DIR}/src/ml_models/cv_LRmodel_results.csv'
    cross_validation_results.to_csv(cv_path, index = False, sep = ';', decimal = ',')
    
    log.info(f"Cross validation finished")
    
    # save confusion matrix
    cm_path = f'{settings.ROOT_DIR}/src/ml_models/cm_LRmodel_results.png'
    cm = confusion_matrix(y_test,y_pred)
    plt.figure(figsize=(15,15))
    sns.heatmap(cm, annot=True,cmap='Blues', fmt='g',xticklabels=(classes),yticklabels=(classes))
    plt.title('Confusion Matrix')
    plt.savefig(cm_path)
    
    # final training
    log.info(f"Training final model")
    logreg.fit(X, y)
    # save model pipeline
    lr_model_path = f'{settings.ROOT_DIR}/src/ml_models/lr_model.joblib'
    dump(logreg, lr_model_path)
    
    print(f'Model trained')
