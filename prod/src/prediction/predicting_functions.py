# api
from fastapi import HTTPException

# general
import numpy as np
import pandas as pd
import re
from unidecode import unidecode

# text processing
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

# ml models
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.utils import class_weight
from joblib import load


# predict model
def predicting_multinomialLR(df, settings):
    
    if df.shape[0] == 0:
        raise HTTPException(status_code=400, detail="No data to predict. Please, enter a date at least 3 months from the current date")
    
    # carregar modelos
    model_path = f'{settings.ROOT_DIR}/src/ml_models/lr_model.joblib'
    lr_model = load(model_path)
    
    # predicting
    X = df.tokens
    y_pred = lr_model.predict(X)
    
    # add dataframe
    df['classificacao'] = y_pred
    
    df = df.reset_index()
    
    return df

# post prediction
def post_processing_model(data_list_to_append, columns):
    
    df = pd.concat(data_list_to_append)
    df = df[columns]
    df = df.fillna('indefinido por falta de preenchimento')
    df = df.reset_index(drop=True)
    df['dataConsulta'] = df['dataConsulta'].dt.strftime('%Y-%m-%d')
    
    return df