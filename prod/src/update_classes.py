from fastapi import HTTPException
from pathlib import Path
from src.utils.gcp_utils import upload_file
import shutil
import pandas as pd


# processo de armazenar o arquivo das classes internamente
def save_upload_classes(file, settings):
    
    # save file internally
    destination = Path(f'{settings.ROOT_DIR}/src/ml_models/dict_pa.csv')
    
    try:
        with destination.open("wb") as buffer:
            shutil.copyfileobj(file.file, buffer)
    finally:
        file.file.close()
        
    print(f'file {file.filename} saved internally')
    
    

# processo de armazenar o arquivo das classes
def run_gcp_upload_classes(file, settings):
    
    source_file = f'{settings.ROOT_DIR}/src/ml_models/dict_pa.csv'
    destination_file = f'{settings.PA_DIGITAL_FOLDER_NAME}/ml_models/dict_pa.csv'
    
    upload_file(bucket_name = settings.GCLOUD_BUCKET, 
                destination_blob_name = destination_file, 
                source_file_path = source_file, 
                credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    print(f'file {file.filename} uploaded to gcp')
    
    
    
# rodar o processo
def run(settings, file):
    
    # validate file extension
    if file.filename.endswith(('.csv', '.xlsx')) != True:
        raise HTTPException(400, detail="Invalid document type")
        
    # save internally
    save_upload_classes(file, settings)
    
    # validate file content
    source_file = f'{settings.ROOT_DIR}/src/ml_models/dict_pa.csv'
    if pd.read_csv(source_file).shape[0] <= 0:
        raise HTTPException(400, detail="empty .csv file")
    
    # upload storage
    run_gcp_upload_classes(file, settings) 
        
    print('Upload success')
