from src.data_prep.data_prep_utils import read_query, bq_query_to_df, format_df, cleaning_df, preprocessing_df
from src.training.training_functions import create_target, training_multinomialLR_process
from src.utils.gcp_utils import upload_file, download_blob
import pandas as pd



# run download dictionary
def run_gcp_download_dictionary(settings):
    
    source_file = f'{settings.PA_DIGITAL_FOLDER_NAME}/ml_models/dict_pa.csv'
    destination_file = f'{settings.ROOT_DIR}/src/ml_models/dict_pa.csv'
    
    download_blob(bucket_name = settings.GCLOUD_BUCKET, 
                  source_blob_name = source_file, 
                  destination_file_name = destination_file, 
                  credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    print(f'file dict_pa.csv downloaded from gcp')

    
    
# upload metrics to gcp
def run_gcp_upload_metrics(settings):
    
    # source files
    source_file_cross_val = f'{settings.ROOT_DIR}/src/ml_models/cv_LRmodel_results.csv'
    source_file_confusion_matrix = f'{settings.ROOT_DIR}/src/ml_models/cm_LRmodel_results.png'
    source_files = [source_file_cross_val, source_file_confusion_matrix]
    
    # destination files
    destination_file_cross_val = f'{settings.PA_DIGITAL_FOLDER_NAME}/ml_results/cv_LRmodel_results.csv'
    destination_file_confusion_matrix = f'{settings.PA_DIGITAL_FOLDER_NAME}/ml_results/cm_LRmodel_results.png'
    destination_files = [destination_file_cross_val, destination_file_confusion_matrix]
    
    # upload files
    for source, destination in zip(source_files, destination_files):
        
        upload_file(bucket_name = settings.GCLOUD_BUCKET, 
                    destination_blob_name = destination, 
                    source_file_path = source, 
                    credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
        
    print(f'ML metrics results uploaded to gcp')

    
        
# upload model to gcp
def run_gcp_upload_model(settings):
    
    # upload cross validation
    source_file = f'{settings.ROOT_DIR}/src/ml_models/lr_model.joblib'
    destination_file = f'{settings.PA_DIGITAL_FOLDER_NAME}/ml_models/lr_model.joblib'
    
    upload_file(bucket_name = settings.GCLOUD_BUCKET, 
                destination_blob_name = destination_file, 
                source_file_path = source_file, 
                credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    print(f'ML model uploaded to gcp')
    
    
    
# processo preprocessar textos
def preproc_pa_digital(df):
    
    # format df
    formated_df = format_df(df, columns = ['idBeneficiario', 'idConsulta'])
    # clean df
    cleaned_df = cleaning_df(formated_df, column_to_clean = 'motivoConsulta')
    # preprocessing df
    processed_df = preprocessing_df(cleaned_df, text_column = 'motivoConsulta')
    
    return processed_df



# processo para treinar o algoritmo do pa digital
def train_proc_pa_digital(processed_df, settings):
    
    # dictionary load
    ## baixar dicionario
    run_gcp_download_dictionary(settings)
    dict_pa = pd.read_csv(f'{settings.ROOT_DIR}/src/ml_models/dict_pa.csv').fillna('-')
    
    # create target classes
    final_df = create_target(processed_df, dictionary=dict_pa)
    
    # train model
    training_multinomialLR_process(final_df, settings)
    
    print("Model trainning success")
    
    
    
# rodar os processos
def run(settings):
    
    # data aquisition
    query_path = f'{settings.ROOT_DIR}/src/data_prep/queries/query_pa_digital_train.txt'
    sql_query = read_query(query_path)
    
    df = bq_query_to_df(query = sql_query, 
                        project = settings.GCLOUD_PROJECT, 
                        dataset = settings.BIGQUERY_DATASET_NAME, 
                        table = settings.BIGQUERY_DATASET_TABLE,
                        credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    # preprocessing
    processed_df = preproc_pa_digital(df)
    
    # training
    print('Start training...')
    train_proc_pa_digital(processed_df, settings)
    
    # save model joblib
    run_gcp_upload_model(settings)
    
    # save metrics
    run_gcp_upload_metrics(settings)
    
    print('Run trainning finished')