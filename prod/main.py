from fastapi import FastAPI
from routers import router

# fastapi app
app = FastAPI(
    title = "PA Digital",
    description="Um algoritmo para classificar textos de atendimentos digitais",
    version="0.1"
)

app.include_router(router)