# pytest tests/main_tests.py

from starlette.testclient import TestClient
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from main import app


client = TestClient(app)

# testar /home_page
def test_home_page():
    response = client.get("/")
    assert response.status_code == 200
    
    
# testar /load_classes
def test_load_classes_1():
    test_file = open("/home/jupyter/pa_digital_project/dev/data/dict_pa.csv", "rb")
    response = client.post("/v1/load_classes", 
                           data={"environment":"dev"},
                           files={"file":test_file})
    assert response.status_code == 200

    
def test_load_classes_2():
    test_file = open("/home/jupyter/pa_digital_project/dev/data/dict_pa.csv", "rb")
    response = client.post("/v1/load_classes", 
                           data={"environment":"prod"},
                           files={"file":test_file})
    assert response.status_code == 200

    
def test_load_classes_3():
    test_file = open("/home/jupyter/pa_digital_project/dev/data/dict_pa.csv", "rb")
    response = client.post("/v1/load_classes", 
                           data={"environment":"jow"},
                           files={"file":test_file})
    assert response.status_code == 400

    
def test_load_classes_4():
    test_file = open("/home/jupyter/pa_digital_project/dev/data/plan_algos.jpeg", "rb")
    response = client.post("/v1/load_classes", 
                           data={"environment": "dev"},
                           files={"file": test_file})
    assert response.status_code == 400

    
# testar pa_train
def test_pa_train_1():
    response = client.post("/v1/pa_train", 
                           data={"environment": "dev"})
    assert response.status_code == 200

def test_pa_train_2():
    response = client.post("/v1/pa_train", 
                           data={"environment": "jow"})
    assert response.status_code == 400

    
# testar pa_predict
def test_pa_predict_1():
    response = client.post("/v1/pa_predict", 
                           data={"environment": "dev",
                                 "empresa_id":159})
    assert response.status_code == 200

    
def test_pa_predict_2():
    response = client.post("/v1/pa_predict", 
                           data={"environment": "dev",
                                 "empresa_id":159,
                                 "filter_appointment_date":"2021-05-13"})
    assert response.status_code == 200

    
def test_pa_predict_3():
    response = client.post("/v1/pa_predict", 
                           data={"environment": "dev",
                                 "empresa_id":"aaa",
                                 "filter_appointment_date":"2021-05-13"})
    assert response.status_code == 422

    
def test_pa_predict_4():
    response = client.post("/v1/pa_predict", 
                           data={"environment": "dev",
                                 "empresa_id":159,
                                 "filter_appointment_date":"13/05/2021"})
    assert response.status_code == 422

    
def test_pa_predict_5():
    response = client.post("/v1/pa_predict", 
                           data={"environment": "dev",
                                 "empresa_id":159,
                                 "filter_appointment_date":"2021-12-30"})
    assert response.status_code == 400
    
def test_pa_predict_6():
    response = client.post("/v1/pa_predict", 
                           data={"environment": "dev",
                                 "empresa_id":555555,
                                 "filter_appointment_date":"2021-05-13"})
    assert response.status_code == 400
    
    
# testar preview_dictionary
def test_preview_dictionary_1():
    response = client.get("/v1/preview_dictionary/dev")
    assert response.status_code == 200

def test_preview_dictionary_2():
    response = client.get("/v1/preview_dictionary/prod")
    assert response.status_code == 200

def test_preview_dictionary_3():
    response = client.get("/v1/preview_dictionary/prood")
    assert response.status_code == 400




# testar preview_crossvalidation
def test_preview_crossvalidation_1():
    response = client.get("/v1/preview_crossvalidation/dev")
    assert response.status_code == 200

def test_preview_crossvalidation_2():
    response = client.get("/v1/preview_crossvalidation/prod")
    assert response.status_code == 200

def test_preview_crossvalidation_3():
    response = client.get("/v1/preview_crossvalidation/prood")
    assert response.status_code == 400

    
    
# testar preview_confusionmatrix
def test_preview_confusionmatrix_1():
    response = client.get("/v1/preview_confusionmatrix/dev")
    assert response.status_code == 200

def test_preview_confusionmatrix_2():
    response = client.get("/v1/preview_confusionmatrix/prod")
    assert response.status_code == 200

def test_preview_confusionmatrix_3():
    response = client.get("/v1/preview_confusionmatrix/prood")
    assert response.status_code == 400
    
    
    
    
    
# curl's load_classes
# curl -v POST -F 'environment=dev' -F 'file=@/home/jupyter/pa_digital_project/dev/data/dict_pa.csv' http://127.0.0.1:8000/v1/load_classes

# curl -v POST -F 'environment=prod' -F 'file=@/home/jupyter/pa_digital_project/dev/data/dict_pa.csv' http://127.0.0.1:8000/v1/load_classes

# curl -v POST -F 'environment=jow' -F 'file=@/home/jupyter/pa_digital_project/dev/data/dict_pa.csv' http://127.0.0.1:8000/v1/load_classes

# curl -v POST -F 'environment=dev' -F 'file=@/home/jupyter/pa_digital_project/dev/data/plan_algos.jpeg' http://127.0.0.1:8000/v1/load_classes


# curl's pa_train
# curl -v POST -F 'environment=dev' http://127.0.0.1:8000/v1/pa_train

# curl -v POST -F 'environment=prod' http://127.0.0.1:8000/v1/pa_train

# curl -v POST -F 'environment=jow' http://127.0.0.1:8000/v1/pa_train



# curl's predict

# curl -v POST -F 'environment=dev' -F 'empresa_id=159' http://127.0.0.1:8000/v1/pa_predict

# curl -v POST -F 'environment=dev' -F 'empresa_id=159' -F 'filter_appointment_date=2021-05-13' http://127.0.0.1:8000/v1/pa_predict

# curl -v POST -F 'environment=dev' -F 'empresa_id=aaa' http://127.0.0.1:8000/v1/pa_predict

# curl -v POST -F 'environment=dev' -F 'empresa_id=159' -F 'filter_appointment_date=13/05/2021' http://127.0.0.1:8000/v1/pa_predict

# curl -v POST -F 'environment=dev' -F 'empresa_id=159' -F 'filter_appointment_date=2021-12-30' http://127.0.0.1:8000/v1/pa_predict

# curl -v POST -F 'environment=dev' -F 'empresa_id=555555' -F 'filter_appointment_date=2021-05-13' http://127.0.0.1:8000/v1/pa_predict


# curl's get dictionary
# curl -v http://127.0.0.1:8000/v1/preview_dictionary/dev

# curl's get cv results
# curl -v http://127.0.0.1:8000/v1/preview_crossvalidation/dev

# curl's get cm results
# curl -v http://127.0.0.1:8000/v1/preview_confusionmatrix/dev



# cloud run
# curl -v POST -F 'environment=dev' -F 'file=@/home/jupyter/pa_digital_project/dev/data/dict_pa.csv' https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/load_classes

# curl -v POST -F 'environment=dev' https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/pa_train

# curl -v POST -F 'environment=dev' -F 'empresa_id=159' -F 'filter_appointment_date=2021-05-13' https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/pa_predict
